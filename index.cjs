const button = document.getElementById("toggle-space");
button.addEventListener("click", priceChange);

function priceChange() {
    let monthlyPrice = document.getElementsByClassName("monthly");
    let annualPrice = document.getElementsByClassName("annually");

    Array(monthlyPrice.length).fill(0).map((element, index) => {
        if (monthlyPrice[index].style.display === "none") {
            monthlyPrice[index].style.display = "flex";
            annualPrice[index].style.display = "none";
            button.style.justifyContent = "flex-end";
        } else {
            monthlyPrice[index].style.display = "none";
            annualPrice[index].style.display = "flex";
            button.style.justifyContent = "flex-start";
        }
    });
}

